<?php
namespace TkachInc\SudokuGenerator;

use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * Class FieldGenerator
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FieldGenerator
{
	protected $field = [
		1, 2, 3, 4, 5, 6, 7, 8, 9,
		4, 5, 6, 7, 8, 9, 1, 2, 3,
		7, 8, 9, 1, 2, 3, 4, 5, 6,

		2, 3, 4, 5, 6, 7, 8, 9, 1,
		5, 6, 7, 8, 9, 1, 2, 3, 4,
		8, 9, 1, 2, 3, 4, 5, 6, 7,

		3, 4, 5, 6, 7, 8, 9, 1, 2,
		6, 7, 8, 9, 1, 2, 3, 4, 5,
		9, 1, 2, 3, 4, 5, 6, 7, 8,
	];
	protected $hard = [
		0, 0, 0, 3, 0, 0, 4, 0, 0,
		9, 0, 0, 0, 1, 0, 0, 0, 0,
		4, 0, 8, 0, 7, 0, 5, 0, 3,

		0, 0, 0, 6, 0, 0, 9, 0, 0,
		0, 0, 0, 1, 5, 7, 0, 0, 0,
		0, 0, 5, 0, 0, 2, 0, 0, 0,

		2, 0, 4, 0, 0, 0, 6, 0, 5,
		0, 0, 0, 0, 2, 0, 0, 0, 8,
		0, 0, 6, 7, 0, 8, 0, 0, 0,
	];
	protected $medium = [
		0, 0, 0, 0, 0, 6, 0, 2, 0,
		0, 3, 0, 7, 0, 8, 4, 0, 0,
		0, 5, 0, 0, 0, 0, 1, 0, 0,

		4, 0, 0, 0, 2, 0, 0, 0, 3,
		6, 0, 0, 0, 0, 0, 0, 0, 8,
		7, 0, 0, 0, 9, 0, 0, 0, 2,

		3, 0, 1, 0, 0, 0, 0, 5, 0,
		0, 0, 9, 3, 0, 5, 0, 7, 0,
		5, 6, 0, 4, 0, 0, 0, 0, 0,
	];
	protected $easy = [
		9, 0, 8, 2, 0, 0, 0, 6, 0,
		7, 0, 5, 1, 0, 3, 0, 0, 0,
		0, 5, 0, 0, 8, 0, 1, 0, 7,

		2, 0, 0, 0, 0, 9, 6, 0, 0,
		0, 0, 6, 0, 2, 0, 9, 0, 0,
		0, 0, 3, 7, 0, 0, 0, 0, 2,

		4, 0, 7, 0, 6, 0, 0, 0, 0,
		0, 0, 0, 4, 0, 2, 7, 0, 6,
		0, 6, 0, 0, 0, 7, 0, 0, 4,
	];
	protected $simple = [
		0, 0, 7, 0, 0, 9, 0, 0, 0,
		0, 6, 0, 8, 0, 0, 0, 1, 0,
		0, 9, 0, 2, 3, 0, 0, 0, 8,

		0, 0, 0, 7, 0, 4, 6, 0, 0,
		7, 0, 6, 0, 0, 0, 9, 0, 3,
		0, 0, 5, 6, 0, 3, 0, 0, 0,

		6, 0, 0, 0, 8, 1, 0, 9, 0,
		0, 1, 0, 0, 0, 2, 0, 4, 0,
		0, 0, 0, 3, 0, 0, 5, 6, 0,
	];

	/**
	 * FieldGenarator constructor.
	 *
	 * @param string $emptySymbol
	 * @throws \Exception
	 */
	public function __construct($emptySymbol = '0')
	{
		$this->emptySymbol = $emptySymbol;
	}

	/**
	 * @return $this
	 */
	public function mapping()
	{
		$map = [
			1 => 10,
			2 => 10,
			3 => 10,
			4 => 10,
			5 => 10,
			6 => 10,
			7 => 10,
			8 => 10,
			9 => 10,
		];
		for ($i = 0; $i < 4; $i++) {
			$symToReplace = ArrayHelper::weightedIndex($map);
			unset($map[$symToReplace]);
			$symFromReplace = ArrayHelper::weightedIndex($map);
			unset($map[$symFromReplace]);

			foreach ($this->field as $p => $sim) {
				if ($symToReplace === $sim) {
					$this->field[$p] = $symFromReplace;
				} elseif ($symFromReplace === $sim) {
					$this->field[$p] = $symToReplace;
				}
			}
		}

		return $this;
	}

	/**
	 * @param int $count
	 * @return $this
	 * @throws \Exception
	 */
	public function shuffle($count = 10)
	{
		$methods = [
			'transposing'     => 1,
			'swapRowsSmall'   => 1,
			'swapColumsSmall' => 1,
			'swapRowsArea'    => 1,
			'swapColumsArea'  => 1,
		];
		while ($count > 0 && $count < 100) {
			$method = ArrayHelper::weightedIndex($methods);
			if (!method_exists($this, $method)) {
				throw new \Exception('Not found method');
			}
			$this->{$method}();
			$count--;
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function transposing()
	{
		$result = [];
		$k = 0;
		for ($i = 0; $i < 9; $i++) {
			for ($j = 0; $j < 9; $j++) {
				//echo $k;
				//echo ' ';
				//echo $j*9+$i;
				//echo PHP_EOL;
				$result[$k] = $this->field[$j * 9 + $i];
				$k++;
			}
		}
		$this->field = $result;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function swapRowsSmall()
	{
		$area = mt_rand(0, 2);
		$chanse = [0 => 1, 1 => 1, 2 => 1];
		$row = ArrayHelper::weightedIndex($chanse);
		unset($chanse[$row]);
		$row2 = ArrayHelper::weightedIndex($chanse);

		$area = 3 * $area;
		$row = ($area + $row) * 9;
		$row2 = ($area + $row2) * 9;

		for ($i = 0; $i < 9; $i++) {
			$tmp = $this->field[$row + $i];
			$this->field[$row + $i] = $this->field[$row2 + $i];
			$this->field[$row2 + $i] = $tmp;
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function swapColumsSmall()
	{
		$this->transposing();
		$this->swapRowsSmall();
		$this->transposing();

		return $this;
	}

	/**
	 * @return $this
	 */
	public function swapRowsArea()
	{
		$area = mt_rand(0, 2);
		$chanse = [0 => 1, 1 => 1, 2 => 1];
		unset($chanse[$area]);
		$area2 = ArrayHelper::weightedIndex($chanse);
		$area = 3 * $area;
		$area2 = 3 * $area2;

		for ($j = 0; $j < 3; $j++) {
			$row = ($area + $j) * 9;
			$row2 = ($area2 + $j) * 9;

			for ($i = 0; $i < 9; $i++) {
				$tmp = $this->field[$row + $i];
				$this->field[$row + $i] = $this->field[$row2 + $i];
				$this->field[$row2 + $i] = $tmp;
			}
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function swapColumsArea()
	{
		$this->transposing();
		$this->swapRowsArea();
		$this->transposing();

		return $this;
	}

	/**
	 * @return $this
	 */
	public function mirrorField()
	{
		for ($r = 0; $r < 4; $r++) {
			for ($f = 0; $f < 9; $f++) {
				$tmp = $this->field[($f * 9) + $r];
				$this->field[($f * 9) + $r] = $this->field[($f * 9) + (8 - $r)];
				$this->field[($f * 9) + (8 - $r)] = $tmp;
			}
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function get()
	{
		return $this->field;
	}

	/**
	 * @param $difficult
	 * @return $this
	 * @throws \Exception
	 */
	public function getFieldByDifficult($difficult)
	{
		switch ($difficult) {
			case 'hard':
				$this->field = $this->hard;
				$this->shuffle();
				break;
			case 'medium':
				$this->field = $this->medium;
				$this->shuffle();
				break;
			case 'easy':
				$this->field = $this->easy;
				$this->shuffle();
				break;
			case 'simple':
				$this->field = $this->simple;
				$this->shuffle();
				break;
			default:
				$this->shuffle();
				break;
		}

		return $this;
	}

	/**
	 * @param $val
	 * @param $x
	 * @param $y
	 * @return bool
	 */
	public function checkValidity($val, $x, $y)
	{
		for ($i = 0; $i < 9; $i++) {
			if (($this->field[$y * 9 + $i] == $val) || ($this->field[$i * 9 + $x] == $val)) {
				return false;
			}
		}
		$startX = (int)((int)($x / 3) * 3);
		$startY = (int)((int)($y / 3) * 3);

		for ($i = $startY; $i < $startY + 3; $i++) {
			for ($j = $startX; $j < $startX + 3; $j++) {
				if ($this->field[$i * 9 + $j] == $val) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		$str = '';
		foreach ($this->field as $sym) {
			if ($sym == 0) {
				$sym = $this->emptySymbol;
			}
			$str .= $sym;
		}

		return $str;
	}

	/**
	 * @return string
	 */
	public function drawField()
	{
		$str = '';
		foreach ($this->get() as $key => $value) {
			if (($key % 9) === 0) {
				$str .= PHP_EOL;
			}
			$str .= $value . ' ';
		}
		$str .= PHP_EOL;

		return $str;
	}
}